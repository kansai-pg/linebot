<?php

/**
 * Copyright 2016 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/*
 * This polyfill of hash_equals() is a modified edition of https://github.com/indigophp/hash-compat/tree/43a19f42093a0cd2d11874dff9d891027fc42214
 *
 * Copyright (c) 2015 Indigo Development Team
 * Released under the MIT license
 * https://github.com/indigophp/hash-compat/blob/43a19f42093a0cd2d11874dff9d891027fc42214/LICENSE
 */
if (!function_exists('hash_equals')) {
    defined('USE_MB_STRING') or define('USE_MB_STRING', function_exists('mb_strlen'));

    /**
     * @param string $knownString
     * @param string $userString
     * @return bool
     */
    function hash_equals($knownString, $userString)
    {
        $strlen = function ($string) {
            if (USE_MB_STRING) {
                return mb_strlen($string, '8bit');
            }

            return strlen($string);
        };

        // Compare string lengths
        if (($length = $strlen($knownString)) !== $strlen($userString)) {
            return false;
        }

        $diff = 0;

        // Calculate differences
        for ($i = 0; $i < $length; $i++) {
            $diff |= ord($knownString[$i]) ^ ord($userString[$i]);
        }
        return $diff === 0;
    }
}

class LINEBotTiny
{
    /** @var string */
    private $header;

    public function __construct()
    {
	$this->header = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . getenv('LINE_CHANNEL_ACCESS_TOKEN'),
        );
    }

    /**
     * @return mixed
     */
    public function parseEvents()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            http_response_code(405);
            error_log('Method not allowed');
            exit();
        }

        $entityBody = file_get_contents('php://input');

        if ($entityBody === false || strlen($entityBody) === 0) {
            http_response_code(400);
            error_log('Missing request body');
            exit();
        }

        if (!hash_equals($this->sign($entityBody), $_SERVER['HTTP_X_LINE_SIGNATURE'])) {
            http_response_code(400);
            error_log('Invalid signature value');
            exit();
        }

        $data = json_decode($entityBody, true);
        if (!isset($data['events'])) {
            http_response_code(400);
            error_log('Invalid request body: missing events property');
            exit();
        }
        return $data['events'];
    }

    /**
     * @param array<string, mixed> $message
     * @return void
     */

      public function context_create($method, $contents)
      {
        $context = stream_context_create([
            'http' => [
                'ignore_errors' => true,
                'method' => $method,
                'header' => implode("\r\n", $this->header),
                'content' => json_encode($contents),
            ],
        ]);
        return $context;
    }
    
    public function call_LINE_api($url, $method, $contents)
    {
      $response = file_get_contents($url, false, $this->context_create($method, $contents) );
      if (strpos($http_response_header[0], '200') === false)
      {
              error_log('Request failed: ' . $response);
              exit();
      }
      return $response;
    }

    function sendmsg($user_id, $msg, $msg_type)
    {
	if ($msg_type === 'reply') {
		$token_type = 'replyToken';
		$send_type = 'reply';
    	} else{
		$token_type = 'to';
		$send_type = 'push';
	}
        $this->call_LINE_api("https://api.line.me/v2/bot/message/$send_type", 'POST',([
                $token_type => $user_id,
                'messages' => [
                    [
                        'type' => 'text',
                        'text' => $msg
                    ]
                ]
	]) );
	if($msg_type != 'exception') {
		error_log('exit work');
       		exit();
	}
    }
    public function flexMessage($senduser, $message, $is_json)
    {

	if($is_json){
		$json = json_decode( mb_convert_encoding( $message, 'UTF8'),true );
	} else {
		$json = $message;	
	}

	$contents = [
	  'replyToken' => $senduser,
	  "messages" => [
	    [
	      "type" => "flex",
	      "altText" => "This is a Flex Message",
	      "contents" => $json
	    ]
	  ] 
	];

	$this->call_LINE_api('https://api.line.me/v2/bot/message/reply', 'POST', $contents);
	exit();
    }

    /**
     * @param string $body
     * @return string
     */
    private function sign($body)
    {
        $hash = hash_hmac('sha256', $body, getenv('LINE_CHANNEL_SECRET'), true);
        $signature = base64_encode($hash);
        return $signature;
    }

}
