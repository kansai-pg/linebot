<?php
require_once(dirname(__FILE__) . '/database_execute.php');
class account{

	private $dbh;
	private $database;

	public function __construct()
	{
		$this->database = new database_execute();
	} 

	public function insert_1st($user_id, $user_type)
	{
		try {
			if($user_type === 'userId')
			{
				return $this->database->sql("INSERT INTO temp (id, on_duty, user_id, status, datetime, frequency, lastpush ) VALUES ( UUID(), 'NOT', :user_id, 0, DATE_ADD( NOW(), INTERVAL 7 DAY ), 7, NOW() )",[':user_id' => $user_id]);
			
			} else {
				return $this->database->sql('INSERT INTO temp (id, user_id, status, datetime, frequency, lastpush ) VALUES ( UUID(), :user_id, 0, DATE_ADD( NOW(), INTERVAL 7 DAY ), 7, NOW() )',[':user_id' => $user_id]);
			
			}

		} catch (Exception $e) {
			throw $e;
                }
	}
	
	public function select($user_id)
	{
		try {
			$this->database->sql('SELECT * FROM temp WHERE user_id = :user_id',[':user_id' => $user_id]);
			return $this->database->fetchall(); 
		} catch (Exception $e) {
			throw $e;
                }
	}
	
	public function select_main($user_id)
	{
		try {
			$this->database->sql('SELECT * FROM mainid WHERE user_id = :user_id',[':user_id' => $user_id]);
			return $this->database->fetchall(); 
		} catch (Exception $e) {
			throw $e;
                }
	}
	
	public function update($column, $user_id, $data, $column2, $data2 = false)
	{
		try {
			if($column2) {
				$this->database->sql("UPDATE temp SET $column = :data, $column2 = :data2 WHERE user_id = :user_id",['user_id' => $user_id, 'data' => $data, 'data2' => $data2]);
			} else {
				$this->database->sql("UPDATE temp SET $column = :data WHERE user_id = :user_id",['user_id' => $user_id, 'data' => $data]);
			}
		} catch (Exception $e) {
			throw $e;
                }
        }
	
	public function move_main($user_id)
	{

		try {
			return $this->database->sql('INSERT INTO mainid SELECT * FROM temp WHERE user_id = :user_id; DELETE FROM temp WHERE user_id = :user_id',[':user_id' => $user_id]);
		} catch (Exception $e) {
			throw $e;
                }
	}

	public function move_temp($id, $column, $column2 = false)
	{
		try {
			if($column2)
			{
				return $this->database->sql("INSERT INTO temp SELECT * FROM mainid WHERE id = :id; DELETE FROM mainid WHERE id = :id; UPDATE temp SET $column = NULL WHERE id = :id; UPDATE temp SET $column2 = NULL WHERE id = :id",[':id' => $id]);
			} else {
				return $this->database->sql("INSERT INTO temp SELECT * FROM mainid WHERE id = :id; DELETE FROM mainid WHERE id = :id; UPDATE temp SET $column = NULL WHERE id = :id",[':id' => $id]);
			}
		} catch (Exception $e) {
			throw $e;
                }
	}
	
	public function delete_main ($id)
	{
		try {
			return $this->database->sql('DELETE FROM mainid WHERE id = :id',[':id' => $id]);
		} catch (Exception $e) {
			throw $e;
                }
	}

}

