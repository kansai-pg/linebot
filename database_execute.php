<?php
require_once(dirname(__FILE__) . '/LINEBotTiny.php');
class database_execute{

	private $dbh;
	private $stmt;

	public function __construct()
	{
		try {
			$this->dbh = new PDO('mysql:dbname=oqvwr_linebot;host=mysql16.conoha.ne.jp', 'oqvwr_linebot', getenv('DBPASS') );
		} catch (Exception $e) {
			throw $e;
  		}
	}

	public function get_dbh()
	{
		return $this->dbh;
	}

	public function sql($sql, $bindvalue)
	{
		try {
			$this->stmt = $this->dbh->prepare($sql);
			foreach($bindvalue as $key => $value)
			{
	        		$this->stmt->bindValue($key,$value,PDO::PARAM_STR);
			}
		} catch (Exception $e) {
			throw $e;
  		}
		return $this->stmt->execute();
	}
	public function sql_no_bindValue($sql)
	{
		try {
			$this->stmt = $this->dbh->prepare($sql);
			$this->stmt->execute();
			return $this->stmt->execute();
		} catch (Exception $e) {
			throw $e;
  		}
	}
			
	public function fetchall()
	{	
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);	
	}

}

