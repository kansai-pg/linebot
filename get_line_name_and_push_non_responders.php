<?php
require_once(dirname(__FILE__) . '/LINEBotTiny.php');
require_once(dirname(__FILE__) . '/database_execute.php');
$client = new LINEBotTiny();
$database = new database_execute();

$database->sql_no_bindValue("SELECT on_duty FROM mainid WHERE on_duty != 'NOT' AND on_duty != 'NOT_SET'");
foreach($database->fetchall() as $row)
{
	$api_result = json_decode( $client->call_LINE_api('https://api.line.me/v2/bot/profile/' . $row['on_duty'], 'GET', null) , True);
	$database->sql("INSERT INTO duty (user_id, user_name) VALUES  (:on_duty, :displayName )  ON DUPLICATE KEY UPDATE user_name = :displayName",[':on_duty' => $row['on_duty'], 'displayName' => $api_result['displayName']]);
}

$database->sql_no_bindValue("SELECT user_id, room_name, lastpush, id FROM mainid WHERE LASTPUSH <= DATE_ADD(NOW(), INTERVAL -3 MONTH) AND status != 2");
foreach($database->fetchall() as $row)
{
        $json = json_decode( mb_convert_encoding( file_get_contents(dirname(__FILE__) . '/json/long_term_non_responders.json'), 'UTF8' ),true );
        $json['to'] = $row['user_id'];
        $json['messages'][0]['altText'] = $row['room_name'] . '掃除に関する返答が長期間ありません。';
        $json['messages'][0]['contents']['header']['contents'][0]['text'] = $row['room_name'] . '掃除に関する返答が長期間ありません。';
        $json['messages'][0]['contents']['body']['contents'][0]['text'] = $row['room_name'] . '掃除に関する返答が三ヶ月以上ありません。';
        $json['messages'][0]['contents']['body']['contents'][1]['text'] = '前回の掃除日は' . $row['lastpush'] . 'です';
        $json['messages'][0]['contents']['footer']['contents'][0]['action']['text'] = 'NODEL,' . $row['id'];
	$json['messages'][0]['contents']['footer']['contents'][1]['action']['text'] = 'DEL,' . $row['id'];

	$client->call_LINE_api('https://api.line.me/v2/bot/message/push', 'POST', $json);
}

$database->sql_no_bindValue("UPDATE mainid SET status = 2 WHERE LASTPUSH <= DATE_ADD(NOW(), INTERVAL -3 MONTH) AND status != 2");
