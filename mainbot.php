<?php

/**
 * Copyright 2016 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

require_once(dirname(__FILE__) . '/LINEBotTiny.php');
include(dirname(__FILE__) . '/account_class.php');
$date = new DateTime('now');
$client = new LINEBotTiny();

try {
	$stmt = new account();
} catch( PDOException $e ) {
	throw $e;
}

$no_set_msg = '登録と入力することで登録出来ます';

function insert_1st($stmt, $client, $user_type, $event){
	try {
		if(!$stmt->select($event['source'][$user_type]) )
		{
			$stmt->insert_1st($event['source'][$user_type], $user_type);
			$client->flexMessage($event['replyToken'], file_get_contents(dirname(__FILE__) . '/json/roomoptions.json'),true );
		} 
		$client->sendmsg($event['replyToken'], "すでに待機状態です。このまま入力してください", 'reply');
	} catch( PDOException $e ) {
		$client->sendmsg('Udbcae3a9f7a901d256e0a6d433cc66f0', (String)$e, 'exception');
		$client->sendmsg($event['replyToken'], 'データベース接続エラーです。数分後にお試しください', 'reply');
		
	}
}


function register($stmt, $client, $user_type, $event )
{
	global $date;
	global $no_set_msg;
	try {
		if($stmt->select($event['source'][$user_type])[0]['id'] && !$stmt->select($event['source'][$user_type])[0]['room_name'])
		{
			$stmt->update("room_name", $event['source'][$user_type], $event['message']['text'], false, false);
			$client->flexMessage($event['replyToken'], file_get_contents(dirname(__FILE__) . '/json/time_list.json'),true );
		}

		if($stmt->select($event['source'][$user_type])[0]['room_name'] && !$stmt->select($event['source'][$user_type])[0]['time'])
		{
			if(!ctype_digit( $event['message']['text'] ) )
			{
				$client->sendmsg($event['replyToken'], "数値を入力してください。", 'reply');
			} 
			$stmt->update("time", $event['source'][$user_type], $event['message']['text'], false, false);
			
			if($user_type === "groupId" && !$stmt->select($event['source'][$user_type])[0]['on_duty'])
			{
				$client->flexMessage($event['replyToken'], file_get_contents(dirname(__FILE__) . '/json/msg.json'),true );
			}
			
			$stmt->move_main($event['source'][$user_type]);
			$client->sendmsg($event['replyToken'], "登録完了しました\n 通知頻度は「日時設定」コマンドで指定できます。\n 初期設定では7日後へ設定されています。", 'reply');
			
		}

		if($stmt->select($event['source'][$user_type])[0]['id'] && !$stmt->select($event['source'][$user_type])[0]['frequency'])
		{
			if(!ctype_digit( $event['message']['text'] ) )
			{
				$client->sendmsg($event['replyToken'], "数値を入力してください。", 'reply');
			}
			$date->modify('+' . $event['message']['text'] . 'day');
			$stmt->update("frequency", $event['source'][$user_type], $event['message']['text'], 'datetime', $date->format('y-m-d') );
			$stmt->move_main($event['source'][$user_type]);
			$client->sendmsg($event['replyToken'], "日時変更完了しました。", 'reply');
		}
		
		if($user_type == "userId")
		{
			$client->sendmsg($event['replyToken'], $no_set_msg, 'reply');
		}
		
		if(!$stmt->select($event['source'][$user_type])[0]['on_duty'])
		{
			if($event['message']['text'] == "not") {

				$stmt->update("on_duty", $event['source'][$user_type], "NOT_SET", false, false);
				$stmt->move_main($event['source'][$user_type]);
				$client->sendmsg($event['replyToken'], "当番なしで登録しました。", 'reply');

			} elseif($event['message']['text'] == "me") {
				$stmt->update("on_duty", $event['source'][$user_type], $event['source']['userId'], false, false);
				$stmt->move_main($event['source'][$user_type]);
				$client->sendmsg($event['replyToken'], "当番ありで登録しました。", 'reply');	
			}
		}
	 } catch( PDOException $e ) {
		$client->sendmsg('Udbcae3a9f7a901d256e0a6d433cc66f0', (String)$e, 'exception');
                $client->sendmsg($event['replyToken'], 'データベース接続エラーです。数分後にお試しください', 'reply');

        }
	
}


function room_lists_view($stmt, $client, $user_type, $event)
{
	global $no_set_msg;
	
	$flex_msg = [
	"type" => "carousel",
	"contents" => [] 
	];
	try {
		$database = $stmt->select_main($event['source'][$user_type]);

	 } catch( PDOException $e ) {
		$client->sendmsg('Udbcae3a9f7a901d256e0a6d433cc66f0', (String)$e, 'exception');
                $client->sendmsg($event['replyToken'], 'データベース接続エラーです。数分後にお試しください', 'reply');
        }	
	if(!$database)
	{
		$client->sendmsg($event['replyToken'], $no_set_msg, 'reply');
	}	
	$i = 0;	
	foreach($database as $data)
	{
		array_push($flex_msg['contents'], json_decode( mb_convert_encoding( file_get_contents(dirname(__FILE__) . '/json/room_lists_view.json'), 'UTF8' ),true ) );
		$flex_msg['contents'][$i]['header']['contents'][0]['text'] = '部屋一覧'; 
		$flex_msg['contents'][$i]['body']['contents'][0]['text'] = $data['room_name']; 
		$flex_msg['contents'][$i]['footer']['contents'][0]['text'] = '通知時刻' . $data['time'] . '時' ;
		$i++;
	}	
	$client->flexMessage($event['replyToken'], $flex_msg, false );
}

function room_edit($stmt, $client, $user_type, $event, $sendmsg)
{
	global $no_set_msg;
	
	$flex_msg = [
	"type" => "carousel",
	"contents" => [] 
	];

	try {	
		$database = $stmt->select_main($event['source'][$user_type]);
       
	} catch( PDOException $e ) {
		$client->sendmsg('Udbcae3a9f7a901d256e0a6d433cc66f0', (String)$e, 'exception');
                $client->sendmsg($event['replyToken'], 'データベース接続エラーです。数分後にお試しください', 'reply');
        }
	
	if(!$database)
	{
		$client->sendmsg($event['replyToken'], $no_set_msg, 'reply');
	}	
	$i = 0;	
	foreach($database as $data)
	{
		array_push($flex_msg['contents'], json_decode( mb_convert_encoding( file_get_contents(dirname(__FILE__) . '/json/room_edit.json'), 'UTF8' ),true ) );
		$flex_msg['contents'][$i]['header']['contents'][0]['text'] = '部屋一覧'; 
		$flex_msg['contents'][$i]['body']['contents'][0]['text'] = $data['room_name']; 
		$flex_msg['contents'][$i]['footer']['contents'][0]['action']['label'] = '通知時刻' . $data['time'] . '時';
		$flex_msg['contents'][$i]['footer']['contents'][0]['action']['text'] = $sendmsg . ','. $data['id'];
		$i++;
	}	
	$client->flexMessage($event['replyToken'], $flex_msg, false );
}

foreach ($client->parseEvents() as $event) {
	#https://munyagu.com/2728
	#error_log( print_r( $event, true ), 3, __DIR__ . '/log.txt' );
	#error_log('message type: ' . gettype($event['type']));
	if($event['message']['type'] == "text") {
		if ($event['message']['text'] == "登録") {
			if ($event['source']['type'] == "user") {
				insert_1st($stmt, $client, 'userId', $event);

			} elseif ($event['source']['type'] == "group") {
				insert_1st($stmt, $client, 'groupId', $event);
			}

		} elseif ($event['message']['text'] == "登録一覧") {
			if ($event['source']['type'] == "user") {
				room_lists_view($stmt, $client, 'userId', $event);

			} elseif ($event['source']['type'] == "group") {
				room_lists_view($stmt, $client, 'group', $event);

			}

		} elseif ($event['message']['text'] == "日時設定") {
			if ($event['source']['type'] == "user") {
				room_edit($stmt, $client, 'userId', $event, 'DATA');

			} elseif ($event['source']['type'] == "group") {
				room_edit($stmt, $client, 'groupId', $event, 'DATA');

			}
		} elseif ($event['message']['text'] == "時刻設定") {
			if ($event['source']['type'] == "user") {
				room_edit($stmt, $client, 'userId', $event, 'TIME');

			} elseif ($event['source']['type'] == "group") {
				room_edit($stmt, $client, 'groupId', $event, 'TIME');
			}

		} elseif ($event['message']['text'] == "削除") {
			if ($event['source']['type'] == "user") {
				room_edit($stmt, $client, 'userId', $event, 'DEL');

			} elseif ($event['source']['type'] == "group") {
				room_edit($stmt, $client, 'groupId', $event, 'DEL');
			}
				
		}
		try {	
			if(strpos($event['message']['text'], "DEL") === 0)
			{
				$stmt->delete_main(explode(',', $event['message']['text'])[1] );
				$client->sendmsg($event['replyToken'], "削除完了しました。", 'reply');
			}		
			
			if(strpos($event['message']['text'], "TIME") === 0)
			{
				$stmt->move_temp(explode(',', $event['message']['text'])[1], 'time' );
				$client->flexMessage($event['replyToken'], file_get_contents(dirname(__FILE__) . '/json/time_list.json'),true );
			}		
			
			if(strpos($event['message']['text'], "DATA") === 0)
			{
				$stmt->move_temp(explode(',', $event['message']['text'])[1], 'frequency', "datetime");
				$client->sendmsg($event['replyToken'], "何日ごとに通知するかを数値のみで入力してください \n（例: 一週間ごとの場合 7）\n また7日と指定した場合本日から7日後に通知されます。", 'reply');
			}		
			
			if(strpos($event['message']['text'], "nope") === 0)
			{
				if ($event['source']['type'] == "user") {
			
					$stmt->update("status", $event['source']['userId'], 0, false, false);
				
				} elseif ($event['source']['type'] == "group") {
			
					$stmt->update("status", $event['source']['groupId'], 0, false, false);
				
				}

				$client->sendmsg($event['replyToken'], "次は頑張りましょう！", 'reply');
			}	

			if(strpos($event['message']['text'], "YES") === 0)
			{
				if ($event['source']['type'] == "user") {
					$stmt->update("last_push", $event['source']['userId'], $date->format('y-m-d'), 'status', 0);
				
				} elseif ($event['source']['type'] == "group") {
					$stmt->update("last_push", $event['source']['groupId'], $date->format('y-m-d'), 'status', 0);
				
				}

				$client->sendmsg($event['replyToken'], "お疲れ様です\n 次も頑張りましょう！", 'reply');
			}	
			
			if(strpos($event['message']['text'], "NODEL") === 0)
			{
				if ($event['source']['type'] == "user") {
					
					$stmt->update("lastpush", $event['source']['userId'], $date->format('y-m-d'), 'status', 0);
				
				} elseif ($event['source']['type'] == "group") {
				
					$stmt->update("lastpush", $event['source']['groupId'], $date->format('y-m-d'), 'status', 0);
				
				}
				$client->sendmsg($event['replyToken'], "アクティブへ変更されました", 'reply');
			}	
				
		} catch( PDOException $e ) {
			$client->sendmsg('Udbcae3a9f7a901d256e0a6d433cc66f0', (String)$e, 'exception');
                	$client->sendmsg($event['replyToken'], 'データベース接続エラーです。数分後にお試しください', 'reply');
		}

		if ($event['source']['type'] == "user") {
			register($stmt, $client, 'userId', $event);
		} elseif ($event['source']['type'] == "group") {
			register($stmt, $client, 'groupId', $event);
		} else {
			$client->sendmsg($event['replyToken'], "グループもしくは1:1チャットからご利用ください", 'reply');
		}
	} else {
		error_log('Unsupported message type: ' . $event['message']['type']);
	};
};
