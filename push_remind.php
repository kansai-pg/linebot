<?php
require_once(dirname(__FILE__) . '/LINEBotTiny.php');
require_once(dirname(__FILE__) . '/database_execute.php');
$client = new LINEBotTiny();
$database = new database_execute();

$database->sql_no_bindValue("SELECT mainid.user_id, room_name, frequency, lastpush, mainid.id, user_name FROM mainid LEFT OUTER JOIN duty ON mainid.on_duty = duty.user_id WHERE datetime <= now() AND time <= DATE_FORMAT(NOW(),'%H')");

foreach($database->fetchall() as $row)
{
	$json = json_decode( mb_convert_encoding( file_get_contents(dirname(__FILE__) . '/json/remind.json'), 'UTF8' ),true );
	$json['to'] = $row['user_id'];
	$json['messages'][0]['altText'] = $row['room_name'] . '掃除の時間です';
	$json['messages'][0]['contents']['header']['contents'][0]['text'] = $row['room_name'] . '掃除の時間です';
	$json['messages'][0]['contents']['body']['contents'][0]['text'] = $row['room_name'] . '掃除の時間です';
	$json['messages'][0]['contents']['body']['contents'][1]['text'] = '前回の掃除日は' . $row['lastpush'] . 'です';
	$json['messages'][0]['contents']['footer']['contents'][0]['action']['text'] = 'YES,' . $row['id'];
	$json['messages'][0]['contents']['footer']['contents'][1]['action']['text'] = 'nope,' . $row['id'];
	
	if($row['user_name'])
	{
		$json['messages'][0]['contents']['body']['contents'][4] = [
									'type' => 'text',
									'text' => '当番は\n',
									'align' => 'center',
									'wrap' => True,
									];
		$json['messages'][0]['contents']['body']['contents'][5] = [
									'type' => 'text',
									'text' => $row['user_name'] . 'です。',
									'align' => 'center',
									'wrap' => True,
									]; 
	}
	$client->call_LINE_api('https://api.line.me/v2/bot/message/push', 'POST', $json);
	$database->sql("UPDATE mainid SET status = 1 WHERE id = :id; UPDATE mainid SET datetime = DATE_ADD( NOW(), INTERVAL 7 DAY ) WHERE id = :id", [':frequency' => $row['frequency'],':id' => $row['id']] );
	#エラーは出ないけど日付が更新されない　なんで？
	#$database->sql("UPDATE mainid SET status = 1 AND datetime =  DATE_ADD( NOW(), INTERVAL :frequency DAY ) WHERE id = :id", [':frequency' => $row['frequency'],':id' => $row['id']] );

}
