create table mainid (
		    id BINARY(36) NOT NULL,
		    room_name varchar(128) NOT NULL,
		    on_duty varchar(128) NOT NULL,
		    user_id varchar(35) NOT NULL,
		    time int NOT NULL,
		    status int,
		    datetime date NOT NULL,
		    frequency int NOT NULL,
		    lastpush date NOT NULL,
		    PRIMARY KEY (id)
		    )
