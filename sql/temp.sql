create table temp (
		    id BINARY(36),
		    room_name varchar(128),
		    on_duty varchar(128),
		    user_id varchar(35) NOT NULL,
		    time int,
		    status int,
		    datetime date,
		    frequency int,
		    lastpush date,
		    PRIMARY KEY (id)
		    )
